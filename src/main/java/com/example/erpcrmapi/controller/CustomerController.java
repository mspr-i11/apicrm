package com.example.erpcrmapi.controller;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(path = "/api/customers")
public class CustomerController {

   /* @Autowired
    Customer customer;*/

    String url = "https://615f5fb4f7254d0017068109.mockapi.io/api/v1/customers/";
    @GetMapping
    private List<Object> getAllCustomer(){
        RestTemplate rt = new RestTemplate();

        Object[] customers = rt.getForObject(url, Object[].class);

        return Arrays.asList(customers);
    }

    //creating a get mapping that retrieves the detail of a specific book
    @GetMapping("/{id}")
    private Object getCustomer(@PathVariable int id){
        RestTemplate rt = new RestTemplate();
        String urlCusto = url + id;
        Object customers = rt.getForObject(urlCusto, Object.class);

        return customers;
    }

    @GetMapping("/{id}/orders")
    private Object getOrderForCustomer(@PathVariable int id){
        RestTemplate rt = new RestTemplate();
        String urlCusto = url + id + "/orders";
        Object customers = rt.getForObject(urlCusto, Object.class);

        return customers;
    }
    @GetMapping("/{id}/orders/{orderid}")
    private Object getOrderIdForCustomer(@PathVariable int id,@PathVariable int orderid){
        RestTemplate rt = new RestTemplate();
        String urlCusto = url + id + "/orders" + orderid;
        Object customer = rt.getForObject(urlCusto, Object.class);

        return customer;
    }
    @GetMapping("/{id}/orders/{orderid}/products")
    private Object getProductForOrderIdForCustomer(@PathVariable int id,@PathVariable int orderid){
        RestTemplate rt = new RestTemplate();
        String urlCusto = url + id + "/orders" + orderid + "/products";
        Object customers = rt.getForObject(urlCusto, Object.class);

        return customers;
    }



}
