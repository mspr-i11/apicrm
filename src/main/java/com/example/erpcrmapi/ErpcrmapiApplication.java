package com.example.erpcrmapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ErpcrmapiApplication {

    public static void main(String[] args) {
        SpringApplication.run(ErpcrmapiApplication.class, args);
    }

}
